﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Models
{
    public enum EstadoBuseta
    {
        MAL_ESTADO = 0,
        DISPONIBLE = 1,
        OCUPADA = 2
    }
}
