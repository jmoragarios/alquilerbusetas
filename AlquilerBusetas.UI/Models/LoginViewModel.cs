﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AlquilerBusetas.UI.Models
{
    public class LoginViewModel
    {
        [Display(Name = "USERNAME")]
        public string UserId { get; set; }
        [Display(Name = "PASSWORD")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
