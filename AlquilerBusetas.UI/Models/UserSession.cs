﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Models
{
    public class UserSession
    {
        public string UserCode { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public UserType UserType { get; set; }
        public bool Estado { get; set; }
        public string Telefono { get; set; }
        public byte LoginStatus { get; set; }
    }
}
