﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Models
{
    public enum UserType
    {
        ADMIN = 1,
        AGENT = 2,
        CLIENT = 3
    }
}
