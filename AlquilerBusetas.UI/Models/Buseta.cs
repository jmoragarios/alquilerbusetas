﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Models
{
    public class Buseta
    {
        [Display(Name = "Id")]
        public long Id { get; set; }
        public string Placa { get; set; }
        [Display(Name = "Cantidad de Pasajeros")]
        public int CantidadPasajeros { get; set; }
        public string Marca { get; set; }
        public EstadoBuseta Estado { get; set; }
        [Display(Name = "Reservado por")]
        public string UsrAsignado { get; set; }
        public decimal Precio { get; set; }
    }
}
