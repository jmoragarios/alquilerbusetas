﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlquilerBusetas.UI.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AlquilerBusetas.UI.Controllers
{
    public class OcupadasController : Controller
    {
        private BusetaDAL dbConnections;
        public OcupadasController(IConfiguration config)
        {
            dbConnections = new BusetaDAL(config);
        }
        // GET: Ocupadas
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {
                var busetasLista = dbConnections.ListarBusetas(2);
                return View(busetasLista);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        // GET: Ocupadas/Edit/5
        public IActionResult Devolver(int id)
        {
            dbConnections.EditarBuseta(string.Empty, id, 1);
            return RedirectToAction("Index");
        }

        
    }
}