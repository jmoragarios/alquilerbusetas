﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AlquilerBusetas.UI.Models;
using Microsoft.AspNetCore.Http;
using AlquilerBusetas.UI.Data;
using Microsoft.Extensions.Configuration;

namespace AlquilerBusetas.UI.Controllers
{
    public class HomeController : Controller
    {
        private BusetaDAL dbConnections;
        public HomeController(IConfiguration config )
        {
            dbConnections = new BusetaDAL(config);
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {
                var busetasLista = dbConnections.ListarBusetas(1);
                return View(busetasLista);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpGet]
        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        [HttpPost]
        public IActionResult Create(Buseta nuevoBuseta)
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {
                dbConnections.CrearBuseta(nuevoBuseta);
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public IActionResult MisBusetas()
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {
                string userCode = HttpContext.Session.GetString("UserCode");
                var busetasLista = dbConnections.ListarMisBusetas(userCode);
                return View(busetasLista);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public IActionResult Reservar(int id)
        {
            string userCode = HttpContext.Session.GetString("UserCode");
            dbConnections.EditarBuseta(userCode, id, 2);
            return RedirectToAction("Index");
        }

        public IActionResult Devolver(int id)
        {
            dbConnections.EditarBuseta(string.Empty, id, 1);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
