﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlquilerBusetas.UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AlquilerBusetas.UI.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace AlquilerBusetas.UI.Controllers
{
    public class LoginController : Controller
    {

        private IConfiguration configurationSettings;
        private LoginDAL _dbConnection;

        public LoginController(IConfiguration config)
        {
            this.configurationSettings = config;
            this._dbConnection = new Data.LoginDAL(configurationSettings);
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("UserSession") != null)
            {

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> Onlogin(Models.LoginViewModel user)
        {
            UserSession userSession = await Task.Run(() => _dbConnection.Login(user));
            // add session to httpcontext
            if (userSession != null)
            {
                if (userSession.LoginStatus == 1)
                {
                    HttpContext.Session.SetString("UserCode", userSession.UserCode);
                    var key = "UserSession";
                    var str = JsonConvert.SerializeObject(userSession);
                    HttpContext.Session.SetString(key, str);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["ErrorMessage"] = "Invalid user or password";
                    user.UserId = "";
                    return View("~/Views/Login/Index.cshtml");
                }
            }
            else
            {
                ViewData["ErrorMessage"] = "Invalid user or password";
                return View("~/Views/Login/Index.cshtml");
            }
        }

        public ActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }

    }
}