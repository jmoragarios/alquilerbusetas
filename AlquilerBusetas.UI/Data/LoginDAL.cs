﻿using AlquilerBusetas.UI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Data
{
    public class LoginDAL
    {
        public IConfiguration configurationSettings;

        public LoginDAL(IConfiguration config)
        {
            this.configurationSettings = config;
        }

        public async Task<UserSession> Login(LoginViewModel user)
        {
            string setting = configurationSettings.GetConnectionString("DefaultConnection");
            UserSession userSession = new UserSession();
            SqlConnection db = new SqlConnection(setting);
            db.Open();
            DataSet ds = new DataSet();
            var status = 0;

            String spName = "dbo.[PA_LOGIN]";
            try
            {
                using (SqlCommand cmd = new SqlCommand(spName, db))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@USER_CODE", user.UserId));
                    cmd.Parameters.Add(new SqlParameter("@PASS", user.Password));

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                    if (ds.Tables != null)
                    {
                        status = (int)ds.Tables[0].Rows[0].ItemArray[0];

                        if (status == 1)
                        {
                            DataTable userTable = ds.Tables[1];

                            userSession.UserCode = (string)userTable.Rows[0]["USER_CODE"];
                            userSession.Name = (string)userTable.Rows[0]["NAME"];
                            userSession.Telefono = (string)userTable.Rows[0]["TELEFONO"];
                            userSession.Email = (string)userTable.Rows[0]["EMAIL"];
                            userSession.Estado = (bool)userTable.Rows[0]["ESTADO"];
                            userSession.UserType = (UserType)(byte)(userTable.Rows[0]["NIVEL_USUARIO"]);
                            userSession.LoginStatus = (byte)status;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db = null;
            }

            db = null;

            return userSession;

        }
    }
}
