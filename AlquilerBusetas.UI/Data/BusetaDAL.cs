﻿using AlquilerBusetas.UI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AlquilerBusetas.UI.Data
{
    public class BusetaDAL
    {
        public IConfiguration configurationSettings;

        public BusetaDAL(IConfiguration config)
        {
            this.configurationSettings = config;
        }

        public List<Buseta> ListarBusetas(byte tipo)
        {
            string setting = configurationSettings.GetConnectionString("DefaultConnection");

            List<Buseta> result = new List<Buseta>();

            String spName = "dbo.[PA_LISTAR_BUSETAS]";

            try
            {
                using (SqlConnection db = new SqlConnection(setting))
                {
                    SqlCommand cmd = new SqlCommand(spName, db);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Tipo", tipo));
                    

                    db.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Buseta item = new Buseta();

                        item.Id = (long)dr["ID_BUSETA"];
                        item.Placa = (string)dr["PLACA"];
                        item.Marca = (string)dr["MARCA"];
                        item.Estado = (EstadoBuseta)(byte)dr["ESTADO"];
                        item.CantidadPasajeros =(int)(dr["CANTIDAD_PASAJEROS"]);
                        item.UsrAsignado = dr["USR_ASIGNADO"] != DBNull.Value ? (string)dr["USR_ASIGNADO"] : string.Empty;
                        item.Precio = dr["PRECIO"] != DBNull.Value ? (decimal)dr["PRECIO"] : 0;
                        result.Add(item);
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<Buseta> ListarMisBusetas(string userCode)
        {
            string setting = configurationSettings.GetConnectionString("DefaultConnection");

            List<Buseta> result = new List<Buseta>();

            String spName = "select * from BUSETA WHERE USR_ASIGNADO = @USER_CODE";

            try
            {
                using (SqlConnection db = new SqlConnection(setting))
                {
                    SqlCommand cmd = new SqlCommand(spName, db);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@USER_CODE", userCode));


                    db.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Buseta item = new Buseta();

                        item.Id = (long)dr["ID_BUSETA"];
                        item.Placa = (string)dr["PLACA"];
                        item.Marca = (string)dr["MARCA"];
                        item.Estado = (EstadoBuseta)(byte)dr["ESTADO"];
                        item.CantidadPasajeros = (int)(dr["CANTIDAD_PASAJEROS"]);
                        item.UsrAsignado = dr["USR_ASIGNADO"] != DBNull.Value ? (string)dr["USR_ASIGNADO"] : string.Empty;
                        item.Precio = dr["PRECIO"] != DBNull.Value ? (decimal)dr["PRECIO"] : 0;
                        result.Add(item);
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public void EditarBuseta(string userCode, int id, int estado)
        {
            string setting = configurationSettings.GetConnectionString("DefaultConnection");

            List<Buseta> result = new List<Buseta>();

            String spName = @"UPDATE BUSETA 
                            SET ESTADO = @ESTADO,
                            USR_ASIGNADO = @USER_CODE
                            WHERE ID_BUSETA=@ID";

            try
            {
                using (SqlConnection db = new SqlConnection(setting))
                {
                    SqlCommand cmd = new SqlCommand(spName, db);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@USER_CODE", userCode));
                    cmd.Parameters.Add(new SqlParameter("@ESTADO", estado));
                    cmd.Parameters.Add(new SqlParameter("@ID", id));

                    db.Open();
                    cmd.ExecuteNonQuery();

                    
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CrearBuseta(Buseta nuevaBuseta)
        {
            string setting = configurationSettings.GetConnectionString("DefaultConnection");

            List<Buseta> result = new List<Buseta>();

            String spName = @"INSERT INTO BUSETA VALUES(@PLACA, @CANTIDADPASAJEROS, @ESTADO, @MARCA, '', @PRECIO)";

            try
            {
                using (SqlConnection db = new SqlConnection(setting))
                {
                    SqlCommand cmd = new SqlCommand(spName, db);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@PLACA", nuevaBuseta.Placa));
                    cmd.Parameters.Add(new SqlParameter("@CANTIDADPASAJEROS", nuevaBuseta.CantidadPasajeros));
                    cmd.Parameters.Add(new SqlParameter("@ESTADO", 1));
                    cmd.Parameters.Add(new SqlParameter("@MARCA", nuevaBuseta.Marca));
                    cmd.Parameters.Add(new SqlParameter("@PRECIO", nuevaBuseta.Precio));

                    db.Open();
                    cmd.ExecuteNonQuery();


                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
